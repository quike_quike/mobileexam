package com.example.pg08carlos.examthing;

/**
 * Created by pg08carlos on 19/04/2017.
 */
public class Course {
    public String mName;
    public float mCredit;

    private float minCredit = 65;


    Course(String name, float credit){
        mName = name;
        mCredit = credit;
    }

    public String GetGrade(){
        float range = 100-minCredit;
        float myRange = mCredit - minCredit;
        float something = 4/range;
        float credit = myRange*something;
        String grade = "A";

        switch((int)credit){
            case 4:
                grade="A";
                break;
            case 3:
                grade="B";
                break;
            case 2:
                grade ="C";
                break;
            case 1:
                grade = "D";
                break;
            default:
                grade="F";
        }
        return grade;
    }
}
