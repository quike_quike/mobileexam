package com.example.pg08carlos.examthing;

import java.util.ArrayList;

/**
 * Created by pg08carlos on 19/04/2017.
 */
public class Student {
    String mName;
    int coursesNum;
    ArrayList<Course> courses = new ArrayList<Course>();
    Student(String name){
        mName = name;
        AddRandomCourses(3);
    }

    public void AddCourse(){

    }

    public void AddRandomCourses(int quantity){
        for (int i = 0; i <quantity;i++){
            float credit = 100;//new Random().nextFloat()*100;
            courses.add(new Course("Course"+(i+1),credit));
        }

    }

    public ArrayList<Course> GetCourses(){
        return courses;
    }

    public float GetGPA(){
        float credits = 0;
        for(int i=0; i<courses.size();i++){
            switch(courses.get(i).GetGrade()){
                case "A":
                    credits+=4;
                    break;
                case "B":
                    credits+=3;
                    break;
                case "C":
                    credits+=2;
                    break;
                case "D":
                    credits+=1;
                    break;
            }
        }
        return credits/courses.size();
    }
}
