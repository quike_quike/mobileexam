package com.example.pg08carlos.examthing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Student> students = new ArrayList<Student>();
    int studentsQuantity = 5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Init();
    }

    private void Init(){
        TableLayout table = (TableLayout)findViewById(R.id.table);

        for (int i=0; i<studentsQuantity; i++ ){
            students.add(new Student("student"+(i+1)));
            TableRow row = new TableRow(this);

            TextView text = new TextView(this);
            TextView text2 = new TextView(this);

            text.setText("Name: "+students.get(i).mName);
            text2.setText(" GPA: "+students.get(i).GetGPA());

            row.addView(text);
            row.addView(text2);

            table.addView(row);

            //Sorry for the lack of formatting, I forgot how to use custom rows/cells/whatever
        }
    }
}
